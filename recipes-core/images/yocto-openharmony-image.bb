# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

SUMMARY = "Yocto Openharmony Image"
DESCRIPTION = "An Oniro device running openharmony components"
LICENSE = "Apache-2.0"

require recipes-core/images/oniro-image-base.bb

IMAGE_INSTALL += "dsoftbus-example"
