# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

SUMMARY = "dsoftbus example"
DESCRIPTION = "an example of usage of dsoftbus libraries"
LICENSE = "Apache-2.0"

SRC_OPT_PROTO = "protocol=https"
SRC_OPT_BRANCH = "branch=main"
SRC_OPT_CLONE_DIR = "git/apps/dsoftbus-example"
SRC_OPT_DEST = "destsuffix=${SRC_OPT_CLONE_DIR}"

SRC_OPTIONS = "${SRC_OPT_PROTO};${SRC_OPT_DEST};${SRC_OPT_BRANCH}"
SRC_URI += "git://booting.oniroproject.org/francesco.pham/dsoftbus-example.git;${SRC_OPTIONS}"

# SRCREV = "3d8262ff0e4e36bebce90e017b58b5d030033670"
SRCREV = "${AUTOREV}"
PV = "1.0+git${SRCPV}"

S = "${WORKDIR}/${SRC_OPT_CLONE_DIR}"

LIC_FILES_CHKSUM = "file://${S}/LICENSE;md5=ba963850f6731c74878fe839d227e675"

inherit cmake pkgconfig

DEPENDS:append = "\
	yocto-openharmony \
    "

RDEPENDS:${PN}:append = "\
	yocto-openharmony \
    "
